package WebService::OverheidIO::BAG;
our $VERSION = '1.3';
# ABSTRACT: Query Overheid.IO/BAG via their API

use Moose;
extends 'WebService::OverheidIO';

sub _build_type {
    return 'bag';
}

sub _build_fieldnames {
    my $self = shift;

    return [qw(
        openbareruimtenaam
        huisnummer
        huisnummertoevoeging
        huisletter
        postcode
        woonplaatsnaam
        gemeentenaam
        provincienaam
        locatie
    )];
}

sub _build_queryfields {
    my $self = shift;
    # Be aware that we return an empty array (ref) due to bugs on the OverheidIO side.
    # If used you will get a 500 server error.
    return [];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SYNOPSIS

    use WebService::OverheidIO::BAG;
    my $overheidio = WebService::OverheidIO::BAG->new(
        key => "your developer key",
    );

=head1 DESCRIPTION

Query the Overheid.io BAG endpoints. BAG stands for Basis Administratie
Gebouwen.

=head1 SEE ALSO

L<WebService::OverheidIO>
